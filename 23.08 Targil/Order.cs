﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _23Aug2020
{
    class Order : IComparable<Order>
    {
        public float Total { get; set; }
        private int NumberOfManot { get; set; }
        public float Avg
        {
            get
            {
                return (Total / NumberOfManot); // please implement
            }
        }

        public Order(float total, int numberOfManot)
        {
            Total = total;
            NumberOfManot = numberOfManot;
        }

        public static Order operator +(Order o1, Order o2)
        {
            return new Order(o1.Total + o2.Total, o1.NumberOfManot + o2.NumberOfManot); // please implement
        }
        public static Order operator -(Order o1, Order o2)
        {
            return new Order(o1.Total - o2.Total, o1.NumberOfManot - o2.NumberOfManot); // please implement
        }

        public static Order operator *(Order o1, int x)
        {
            return new Order(o1.Total * x, o1.NumberOfManot * x);
        }

        public static Order operator /(Order o1, int x)
        {
            return new Order(o1.Total / x, o1.NumberOfManot / x);
        }

        public static bool operator ==(Order o1, Order o2)
        {
            if (o1.NumberOfManot == o2.NumberOfManot && o1.Total == o2.Total)
                return true;
            else
                return false;
            // compare total. or total+mispar manot?
        }

        public static bool operator !=(Order o1, Order o2)
        {
            if (o1.NumberOfManot != o2.NumberOfManot || o1.Total != o2.Total)
                return true;
            else
                return false;
        }

        public override string ToString()
        {
            return $"Number of manot:{NumberOfManot}" + " " + $"Total: {Total}"; // implement
        }

        public override bool Equals(object obj)
        {
            //return base.Equals(obj);
            //Check for null and compare run-time types.
            if ((obj == null) || !this.GetType().Equals(obj.GetType()))
            {
                return false;
            }
            else
            {
                Order otherOrder = (Order)obj;
                return this == otherOrder;
            }
            /////////////////////////////////////////
            // elaborated way:
            //if (obj is null)
            //    return false;

            //if (!(obj is Point))
            //    return false;
            //Point other = (Point)obj;
            //return this == other;

        }

        public int CompareTo(Order comparedOrder)
        {
            //Order comparedOrder = (Order)obj;

            // order by number of manot
            return  comparedOrder.NumberOfManot-this.NumberOfManot;

            //public int CompareTo(Person p)
            //{
            //    return this.Id - p.Id;
            //}

        }
    }

    class CompareOrderByTotal : IComparer<Order>
    {
        public int Compare(Order o1, Order o2)
        {
            if (o1.Total > o2.Total)
                return 1;
            if (o1.Total < o2.Total)
                return -1;
            else
                return 0;

        }
    }
}

﻿using _23Aug2020;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _23._08_Targil
{
    class Program
    {
        static void Main(string[] args)
        {
            Order o1 = new Order(100, 3);
            Order o2 = new Order(200, 2);

            Console.WriteLine(o1.ToString());
            Console.WriteLine(o2.ToString());

            Console.WriteLine($"Averages are: {o1.Avg} and {o2.Avg}");

            Order orderPlus = o1 + o2;
            Console.WriteLine($"orderPlus:{orderPlus}");

            Order orderMinus = o1 - o2;
            Console.WriteLine($"orderMinus:{orderMinus}");

            Order orderMultiply = o1 * 5;
            Console.WriteLine($"orderMultiply:{orderMultiply}");

            Order orderDivide = o2 / 2;
            Console.WriteLine($"orderDivide:{orderDivide}");

            bool areOrdersEqual = o1 == o2;
            Console.WriteLine($"Are the orders equal? :{areOrdersEqual}");

            bool areOrdersDifferent = o1 != o2;
            Console.WriteLine($"Are the orders different? :{areOrdersDifferent}");

            if (o1.Equals(o2))
            {
                Console.WriteLine("Both orders are equal");
            }
            else
            {
                Console.WriteLine("The orders are not equal!");
            }

            Console.WriteLine(o1.CompareTo(o2));

          

        }
    }
}
